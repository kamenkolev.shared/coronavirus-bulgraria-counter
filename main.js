const domParser = new DOMParser()
let infections
let deaths

const fetchCount = async () => {
  const res = await fetch(
    'https://www.mh.government.bg/bg/informaciya-za-grazhdani/potvrdeni-sluchai-na-koronavirus-na-teritoriyata-na-r-blgariya/'
  )
  const text = await res.text()
  const doc = domParser.parseFromString(text, 'text/html')
  const infections = doc.querySelectorAll('td')[1].innerText.trim()
  const deaths = doc.querySelectorAll('td')[3].innerText.trim()
  return [infections, deaths]
}

const main = async () => {
  try {
    ;[infections, deaths] = await fetchCount()

    browser.browserAction.setBadgeText({
      text: `${deaths}/${infections}`
    })
  } catch (error) {}
  // 5 minutes
  setTimeout(main, 300000)
}

main()
