# An addon displaying infection and death rates from Corona virus in Bulgaria in a simple manner

## Hosted at [GitLab](https://gitlab.com/kamenkolev.shared/coronavirus-bulgraria-counter)

## Reviewer Notice

The extension makes no use of libraries, a build process, or any other tooling of any sort.
